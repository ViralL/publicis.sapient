import content from '../../app/reducers/content';

jest.mock('../../app/actions/types', () => {
    // temporary
    const types = function() {
        return true;
    };

    return {
        types
    };
});

describe('content', () => {
    it('content default', () => {
        const props = {
            type: '',
            value: {
                data: {},
                isLoading: false,
                isError: false
            }
        };
        const result = content('state', props);
        expect(typeof result).toBe('string');
        expect(result).toBe('state');
    });
    it('content default', () => {
        const props = {
            type: '',
            value: {
                data: {},
                isLoading: false,
                isError: false
            }
        };
        const result = content(undefined, props);
        expect(typeof result).toBe('object');
    });
    it('content CONTENT_REQUEST', () => {
        const props = {
            type: 'CONTENT_REQUEST',
            value: {
                data: {},
                isLoading: false
            }
        };
        const result = content('state', props);
        expect(typeof result).toBe('object');
        expect(result.isLoading).toBe(false);
    });
    it('content CONTENT_SUCCESS', () => {
        const props = {
            type: 'CONTENT_SUCCESS',
            value: {
                data: {},
                isError: true
            }
        };
        const result = content('state', props);
        expect(typeof result).toBe('object');
        expect(result.isError).toBe(true);
    });
});
