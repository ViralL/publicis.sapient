import React from 'react';
import { shallow } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import Index from '../../app/containers';

const copyObject = (obj) => {
    return JSON.parse(JSON.stringify(obj));
};

jest.mock('../../app/actions/contentActions', () => {
    const fetchContent = jest.fn();
    return {
        fetchContent
    };
});

import { fetchContent } from '../../app/actions/contentActions';

const shallowWithStore = (component, store) => {
    const context = {
        store
    };
    return shallow(component, { context });
};

const initialState = {
    content: {
        data: {},
        isLoading: false,
        isError: false
    }
};

const props = {};
describe('<Index />', () => {
    it('should have Index', () => {
        const store = createMockStore(initialState);
        const wrapper = shallowWithStore(<Index {...props} />, store).dive();
        expect(wrapper.length).toEqual(1);
    });
    it('should have Index with isLoading', () => {
        const initState = copyObject(initialState);
        initState.content.isLoading = true;
        const store = createMockStore(initState);
        const wrapper = shallowWithStore(<Index {...props} />, store).dive();
        expect(wrapper.length).toEqual(1);
    });
    it('should have Index with isError', () => {
        const initState = copyObject(initialState);
        initState.content.isError = true;
        const store = createMockStore(initState);
        const wrapper = shallowWithStore(<Index {...props} />, store).dive();
        expect(wrapper.length).toEqual(1);
    });
    it('should have componentWillMount', () => {
        const store = createMockStore(initialState);
        const wrapper = shallowWithStore(<Index {...props} />, store).dive();
        expect(wrapper.length).toEqual(1);
        wrapper.instance().componentWillMount();
        expect(fetchContent.mock.calls.length).toEqual(5);
    });
});

