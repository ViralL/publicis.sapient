import React from 'react';
import { shallow } from 'enzyme';
import App from '../app/app';

jest.mock('../app/styles/app.scss', () => {
    return true;
});


describe('<App />', () => {
    it('should have App', () => {
        const wrapper = shallow(<App/>);
        expect(wrapper.find('.main')).toHaveLength(1);
    });
});

