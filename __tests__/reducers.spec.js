import rootReducer from '../app/reducers';

jest.mock('../app/reducers/content', () => {
    // temporary
    const reducer = function() {
        return true;
    };
    return {
        reducer
    };
});


describe('index reducer', () => {
    it('rootReducer', () => {
        const result = rootReducer();
        expect(typeof result).toBe('object');
    });
});
