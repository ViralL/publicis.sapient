import React from 'react';
import { shallow } from 'enzyme';
import Slider from '../../../../app/components/Slider/presentation';

const props = {
    data: [
        {
            user: '',
            webformatURL: ''
        }
    ],
    prevArrow: false,
    nextArrow: false,
    getWidth: 100,
    translateX: 0,
    centerPadding: 0,
    slideWidth: 0
};

describe('<Slider />', () => {
    it('should have Slider class', () => {
        const wrapper = shallow(<Slider {...props} />);
        expect(wrapper.find('.slider')).toHaveLength(1);
    });
});

