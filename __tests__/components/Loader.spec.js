import React from 'react';
import { shallow } from 'enzyme';
import Loader from '../../app/components/Loader';


describe('<Loader />', () => {
    it('should have Footer class', () => {
        const wrapper = shallow(<Loader/>);
        expect(wrapper.find('.lds-ellipsis')).toHaveLength(1);
    });
});

