import React from 'react';
import { shallow } from 'enzyme';
import Footer from '../../app/components/Footer';


describe('<Footer />', () => {
    it('should have Footer class', () => {
        const wrapper = shallow(<Footer/>);
        expect(wrapper.find('.footer')).toHaveLength(1);
    });
});

