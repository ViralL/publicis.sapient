import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../app/components/Header';


describe('<Header />', () => {
    it('should have Footer class', () => {
        const wrapper = shallow(<Header/>);
        expect(wrapper.find('.header')).toHaveLength(1);
    });
});

