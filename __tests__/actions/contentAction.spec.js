import { fetchContent } from '../../app/actions/contentActions';
import axios from 'axios';

jest.mock('axios', () => {
    return {
        get: jest.fn()
    };
});

let dispatchObject = null;
const dispatch = (obj) => {
    dispatchObject = obj;
};

describe('contentActions.js test', () => {
    it('contentActions then', () => {
        axios.get.mockResolvedValue({ value: [{ data: 'Data' }] });
        const result = fetchContent('payload');
        expect(typeof result).toBe('function');
        result(dispatch);
        expect(typeof dispatchObject).toBe('object');
        expect(dispatchObject.type).toBe('CONTENT_REQUEST');
        expect(axios.get).toHaveBeenCalledTimes(1);
        expect(axios.get).toHaveBeenCalledWith('https://pixabay.com/api/?key=9656065-a4094594c34f9ac14c7fc4c39&q=beautiful+landscape&image_type=photo');
    });
    it('contentActions then', () => {
        axios.get.mockResolvedValue(Promise.reject());
        const result = fetchContent('payload');
        expect(typeof result).toBe('function');
        result(dispatch);
        expect(typeof dispatchObject).toBe('object');
        expect(dispatchObject.type).toBe('CONTENT_REQUEST');
        expect(axios.get).toHaveBeenCalledTimes(2);
        expect(axios.get).toHaveBeenCalledWith('https://pixabay.com/api/?key=9656065-a4094594c34f9ac14c7fc4c39&q=beautiful+landscape&image_type=photo');
    });
});
