import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchContent } from '../actions/contentActions';

// components
import Slider from '../components/Slider/index';
import Loader from '../components/Loader';

class Index extends Component {

    componentWillMount() {
        this.props.actions.fetchContent();
    }

    render() {
        const { isLoading, isError, content } = this.props;
        return (
            <div className="section background-dark section--lg">
                <div className="container text-center">
                    <h3 className="text-huge text-white text-with-subtitle">Slider</h3>
                    <br />
                    {!isLoading && content ? (
                        <Fragment>
                            <Slider
                                data={content.hits}
                                mobilesSidesToShow={1}
                                tabletSidesToShow={3}
                                slidesToShow={5}
                                centerPadding={10}
                            />
                            {isError === true && (
                                <div className="text-huge">No data</div>
                            )}
                        </Fragment>
                    ) : <Loader />}
                </div>
            </div>
        );
    }
}

Index.propTypes = {
    actions: PropTypes.any,
    isError: PropTypes.bool,
    isLoading: PropTypes.bool,
    content: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        content: state.content.data,
        isLoading: state.content.isLoading,
        isError: state.content.isError
    };
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                fetchContent
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
