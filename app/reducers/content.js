
const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'CONTENT_REQUEST':
            return action.value;
        case 'CONTENT_SUCCESS':
            return action.value;
        default:
            return state;
    }
};

export default reducer;
