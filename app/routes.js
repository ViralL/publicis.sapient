import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Index from './containers/Index';

export default (
	<Switch>
		<Route exact path="/" component={Index} />
	</Switch>
);
