import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import content from './reducers/content';

const rootReducer = combineReducers({
    content,
    routing
});

export default rootReducer;
