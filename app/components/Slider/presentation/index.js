import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Slider = (props) => {
    const containerWidth = props.data.length * props.slideWidth;
    const containerStyle = {
        width: containerWidth,
        transform: `translate(${props.translateX + 'px'}, 0)`
    };
    const buttonPrevClasses = classNames('button', {
        'button-disabled': props.prevArrow,
        'button-primary': !props.prevArrow
    });
    const buttonNextClasses = classNames('button', {
        'button-disabled': props.nextArrow,
        'button-primary': !props.nextArrow
    });
    return (
        <div className="slider" ref={props.getWidth}>
            <div className="slider-container" style={containerStyle}>
                {props.data.map((item, index) => {
                    return (
                        <div key={index} className="slider-item" style={{ width: props.slideWidth - (props.centerPadding * 2) }}>
                            <img src={item.webformatURL} alt={item.user} />
                            <div className="slider-author">{item.user.charAt(0).toUpperCase() + item.user.slice(1).replace(/-|_/g, ' ')}</div>
                        </div>
                    );
                })}
            </div>
            <div className="slider-arrows">
                <div className="prev-arrow">
                    <button className={buttonPrevClasses} disabled={props.prevArrow} onClick={props.goToPrevSlide}>Prev</button>
                </div>
                <div className="next-arrow">
                    <button className={buttonNextClasses} disabled={props.nextArrow} onClick={props.goToNextSlide}>Next</button>
                </div>
            </div>
        </div>
    );
};

Slider.propTypes = {
    translateX: PropTypes.number,
    centerPadding: PropTypes.number,
    slideWidth: PropTypes.number,
    data: PropTypes.array,
    prevArrow: PropTypes.bool,
    nextArrow: PropTypes.bool,
    getWidth: PropTypes.any,
    goToPrevSlide: PropTypes.func,
    goToNextSlide: PropTypes.func
};

export default Slider;
