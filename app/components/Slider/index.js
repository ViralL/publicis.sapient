import React, { Component } from 'react';
import PropTypes from 'prop-types';

// redux
import Presentation from './presentation';

class Slider extends Component {
    constructor(props) {
        super(props);

        this.getWidth = React.createRef();
        this.state = {
            breakPointsSM: 600,
            breakPointsMD: 1024,
            slidesToShow: this.props.slidesToShow || 5,
            slideWidth: 0,
            centerPadding: this.props.centerPadding || 7,
            translateX: 0,
            prevArrow: true,
            nextArrow: false
        };
        this.handleResize = this.handleResize.bind(this);
    }

    componentWillMount() {
        this.updateResponsible();
    }

    componentDidMount() {
        this.getSlideWidth();
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }


    getSlideWidth() {
        this.setState({
            slideWidth: this.getWidth.current.clientWidth / this.state.slidesToShow
        });
    }

    goToNextSlide() {
        this.setState({
            translateX: this.state.translateX - this.state.slideWidth,
            nextArrow: this.state.translateX <= -((this.props.data.length - this.state.slidesToShow - 1) * this.state.slideWidth) ? true : false,
            prevArrow: false
        });
    }

    goToPrevSlide() {
        this.setState({
            translateX: this.state.translateX + this.state.slideWidth,
            prevArrow: this.state.translateX >= -this.state.slideWidth ? true : false,
            nextArrow: false
        });
    }

    ifResponsive(breakpoint) {
        return document.body.clientWidth < this.state[breakpoint];
    }

    updateResponsible() {
        if (this.ifResponsive('breakPointsSM')) {
            this.setState({
                slidesToShow: this.props.mobilesSidesToShow,
                centerPadding: 0
            });
            return true;
        }
        if (this.ifResponsive('breakPointsMD')) {
            this.setState({
                slidesToShow: this.props.tabletSidesToShow,
                centerPadding: this.props.centerPadding
            });
            return true;
        }
        return false;
    }


    handleResize() {
        if (!this.updateResponsible()) {
            this.setState({
                slidesToShow: this.props.slidesToShow,
                centerPadding: this.props.centerPadding,
                prevArrow: true,
                translateX: 0
            });
        }
        this.getSlideWidth();
    }

    render() {
        return (
            <Presentation
                data={this.props.data}
                prevArrow={this.state.prevArrow}
                nextArrow={this.state.nextArrow}
                getWidth={this.getWidth}
                translateX={this.state.translateX}
                slideWidth={this.state.slideWidth}
                centerPadding={this.state.centerPadding}
                goToPrevSlide={this.goToPrevSlide.bind(this)}
                goToNextSlide={this.goToNextSlide.bind(this)}
            />
        );
    }
}

Slider.propTypes = {
    tabletSidesToShow: PropTypes.number,
    mobilesSidesToShow: PropTypes.number,
    slidesToShow: PropTypes.number,
    centerPadding: PropTypes.number,
    data: PropTypes.array
};

export default Slider;
