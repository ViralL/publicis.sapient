
import React from 'react';

const Footer = () =>

    <footer className="footer">
        <div className="container">
            <div className="text-medium">
                © 2018 Trademarks and brands are the property of their respective owners.
            </div>
        </div>
    </footer>;

export default Footer;
