import React, { Fragment } from 'react';
import Routes from './routes';
import Header from './components/Header';
import Footer from './components/Footer';
import './styles/app.scss';

const App = () => {
    return (
        <Fragment>
            <Header />
            <main className="main">
                {Routes}
            </main>
            <Footer />
        </Fragment>
    );
};

export default App;
