import * as types from './types';
import axios from 'axios';

export function fetchContent() {
    return function(dispatch) {
        dispatch({
            type: types.CONTENT_REQUEST,
            value: {
                isLoading: true,
                isError: false
            }
        });
        axios
            .get('https://pixabay.com/api/?key=9656065-a4094594c34f9ac14c7fc4c39&q=beautiful+landscape&image_type=photo')
            .then(({ data }) => {
                dispatch({
                    type: types.CONTENT_SUCCESS,
                    value: {
                        data,
                        isLoading: false,
                        isError: false
                    }
                });
            })
            .catch(() => {
                dispatch({
                    type: types.CONTENT_ERROR,
                    value: {
                        isLoading: false,
                        isError: true
                    }
                });
            });
    };
}
